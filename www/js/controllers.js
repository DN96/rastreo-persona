
angular.module('starter.controllers', [])

.controller('Guardar',
    function($scope,$http,$sce,$ionicPopup, $state){
      var headers= {'Content-Type' : 'application/x-www-form-urlencoded','charset': 'utf-8'};
      $http.defaults.headers.post = headers;
         $scope.data = {}
         $scope.ingreso = function(){
          if ($scope.validar($scope)) {
          var indata = 
            'cedula=' + $scope.data.cedula + '&' 
            + 'nombres='+ $scope.data.nombres+ '&' 
            + 'apellidos='+ $scope.data.apellidos+ '&' 
            + 'genero='+ $scope.data.genero +'&'
            + 'fechaNacimiento='+ $scope.data.FNac.getTime()+'&'
            + 'correo='+$scope.data.correo + '&'
            + 'celular=' + $scope.data.celular + '&'
            + 'clave='+ $scope.data.clave+ '&'
            + 'latitud=' + -0.171486 + '&'
            + 'longitud='+ -78.481408;
            
          $http.post("http://karview.kradac.com:8080/karviewrest/webresources/com.kradac.karview.rest.entites.logic.personas/registro",indata
            )
           .success(function(response){
              var alertPopup = $ionicPopup.alert({
                title: $scope.posts = response.message,
                template:''
            });
              if ($scope.posts != "Ya existe un registro con este número de cédula") {
                 $state.go('app.map'); 
              }
              $scope.data = {}
            });
         }else{
          var alertPopup = $ionicPopup.alert({
                title: 'Llenar los datos',
                template:''
            });
        };
        };
        $scope.validar = function($campo){
          if ($scope.data.cedula == undefined || $scope.data.nombres == undefined|| $scope.data.apellidos == undefined|| $scope.data.genero == undefined
              || $scope.data.FNac == undefined || $scope.data.correo == undefined || $scope.data.celular == undefined || $scope.data.clave == undefined) 
          {
               return false;
          }else{
               return true;
          }
        };
})

.controller('LoginCtrl', function($scope,$http, LoginService, $ionicPopup, $state) {
    var headers= {'Content-Type' : 'application/x-www-form-urlencoded','charset': 'utf-8'};
      $http.defaults.headers.post = headers;
    $scope.data = {};
    $scope.login = function() {
      var datos = 'usuario='+$scope.data.username + '&'
                  + 'clave=' + $scope.data.password;
      $http.post("http://karview.kradac.com:8080/karviewrest/webresources/com.kradac.karview.rest.entites.logic.usuarios/login",datos
            ).success(function(response){
              var alertPopup = $ionicPopup.alert({
                title: $scope.posts = response.message,
                template:''
            });
              console.log($scope.posts);
              if ($scope.posts != "Usuario o contraseña incorrectas.") {
                  $state.go('app.map');
              }
              
              $scope.data = {}
            })
    };

    
})

.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('MapController',
  [ '$scope',
    '$cordovaGeolocation',
    '$stateParams',
    '$ionicModal',
    '$ionicPopup',
    'LocationsService',
    'InstructionsService',
    function(
      $scope,
      $cordovaGeolocation,
      $stateParams,
      $ionicModal,
      $ionicPopup,
      LocationsService,
      InstructionsService
      ) {

      /**
       * Once state loaded, get put map on scope.
       */
     $scope.$on("$stateChangeSuccess", function() {
      $scope.map = {
          defaults: {
            tileLayer: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
            maxZoom: 18,
            zoomControlPosition: 'bottomleft'
          },
          markers : {},
          events: {
            map: {
              enable: ['context'],
              logic: 'emit'
            }
          },
          center : {},

        };
    var tiempo = {timeout: 30000, enableHighAccuracy: false};
      $cordovaGeolocation
      .getCurrentPosition(tiempo)
       .then(
        function (position) {
          $scope.map.center.lat = position.coords.latitude;
          $scope.map.center.lng = position.coords.longitude;
          $scope.map.center.zoom = 15;
          $scope.map.markers.now = {
              lat:position.coords.latitude,
              lng:position.coords.longitude,
              message: "You Are Here",
              focus: true,
              draggable: false
            };
        },
        function (err){
          console.log(err);
          var alertPopup = $ionicPopup.alert({
                title: 'GPS apagado!',
                template: 'Encienda el GPS!'

            });
        }
        );
   var watchOptions = {timeout : 3000, enableHighAccuracy: false};
   var watch = $cordovaGeolocation.watchPosition(watchOptions);
      watch.promise.then( 
     function(position) {
            $scope.map.center.lat = position.coords.latitude;
            $scope.map.center.lng = position.coords.longitude;
            $scope.map.center.zoom = 15;
           $scope.map.markers.now = {
              lat:position.coords.latitude,
              lng:position.coords.longitude,
              message: "You Are Here",
              focus: true,
              draggable: false
            };
      },
       function(err) {
     }
   );
$cordovaGeolocation.clearWatch(watch.promise);
      });
$scope.disabled=true; 

      $scope.locate = function(){
      
     var tiempo = {timeout: 3000, enableHighAccuracy: false};
      $cordovaGeolocation
      .getCurrentPosition()
       .then(
        function (position) {
          $scope.map.center.lat = position.coords.latitude;
          $scope.map.center.lng = position.coords.longitude;
          $scope.map.center.zoom = 15;
          $scope.map.markers.now = {
              lat:position.coords.latitude,
              lng:position.coords.longitude,
              message: "You Are Here",
              focus: true,
              draggable: false
            };
          
        },function (err){
          var alertPopup = $ionicPopup.alert({
                title: 'GPS apagado!',
                template: 'Encienda el GPS!'

            });
        }
        );
   var watchOptions = {timeout : 3000, enableHighAccuracy: false};
   var watch = $cordovaGeolocation.watchPosition(watchOptions);
      watch.promise.then( 
     function(position) {
            $scope.map.center.lat = position.coords.latitude;
            $scope.map.center.lng = position.coords.longitude;
            $scope.map.center.zoom = 15;
           $scope.map.markers.now = {
              lat:position.coords.latitude,
              lng:position.coords.longitude,
              message: "You Are Here",
              focus: true,
              draggable: false
            };
      },
      function(err) {
      }
   );
$cordovaGeolocation.clearWatch(watch.promise);
};
     $scope.Iniciar = function(){
        if ($scope.disabled==true) {
          var latitud = $scope.map.center.lat;
          var longitud = $scope.map.center.lng;
          console.log("Inicia");
          console.log( "latitud: "+latitud);
          console.log( "longitud: "+longitud);
          
          $scope.disabled=false;  
        }else{
          var latitud = $scope.map.center.lat;
          var longitud = $scope.map.center.lng;
          console.log("Termina");
          console.log( "latitud: "+latitud);
          console.log( "longitud: "+longitud);
          
          $scope.disabled=true;  
        };
      };
    }]);